import java.util.Scanner;

public class mainApp {

	public static void main(String[] args) {

		Scanner teclado = new Scanner(System.in);
			
		System.out.println("Introduce de que forma geometrica quieres calcular el area");
		String forma = teclado.next().toLowerCase();
		
		switch (forma) {
		case "circulo":
			System.out.println("introduce el Radio");
			System.out.println(areaCirculo(teclado.nextDouble()));
			break;
			
		case "cuadrado":
			System.out.println("introduce la base");
			double base = teclado.nextDouble();
			System.out.println("introduce la Altura");
			double altura = teclado.nextDouble();
			System.out.println(areaCuadrado(base, altura));
			break;
			
		case "triangulo":
			System.out.println("introduce la base");
			double baseT = teclado.nextDouble();
			System.out.println("introduce la Altura");
			double alturaT = teclado.nextDouble();
			
			System.out.println(areaTriangulo(baseT, alturaT));
			break;
			
		default:
			System.out.println("No existe esa forma");
			break;
		}
	}
	
	public static double areaCirculo(double radio) {
		
		return (Math.pow(radio, 2)*Math.PI);
	}
	
	public static double areaCuadrado(double base, double altura) {
		return base*altura;
	}
	
	public static double areaTriangulo(double base, double altura) {
		return base*altura/2;
	}
}
