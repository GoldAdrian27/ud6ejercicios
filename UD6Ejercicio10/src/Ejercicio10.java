import java.util.Arrays;
import java.util.Scanner;

import com.sun.xml.internal.bind.v2.runtime.unmarshaller.XsiNilLoader.Array;

public class Ejercicio10 {

	public static void main(String[] args) {
		Scanner teclado = new Scanner(System.in);
		System.out.println("introduce el tama�o del array");
		int num[] = new int[teclado.nextInt()];
		rellenarArray(num);
		mayorNumero(num);
		

	}
	public static void rellenarArray(int num[]) {
		int count = 0;
		while(count < num.length) {
			int randomNum = randomNum(1, 50);
			if(esPrimo(randomNum)) {
				num[count] = randomNum;
				count++;
			}
		}

	}
	
	public static boolean esPrimo(int num) {
		int contador = 2;
		
		while ((contador < num)){

			if (num % contador == 0) {

				return false;
			}
			contador++;
		}
		return true;
	}

	private static int randomNum(int min, int max) {
		return (int)(Math.random() * ((max - min) + 1)) + min;
	}

	public static void mayorNumero(int num[]) {
		Arrays.sort(num);

		System.out.println("El numero mas grande primo es: " + num[num.length-1]);
	}
}
