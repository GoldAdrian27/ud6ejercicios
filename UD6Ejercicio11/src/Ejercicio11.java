import java.util.Scanner;

public class Ejercicio11 {

	public static void main(String[] args) {
		Scanner teclado = new Scanner(System.in);
		System.out.println("Introduce el tama�o del array");
		int tama�o = teclado.nextInt();
		int array1[] = new int[tama�o];

		rellenar(array1);
		int array2[] = new int[tama�o];

		array2 = array1;
		array1 = new int[tama�o];
		rellenar(array1);

		int array3[] = multiplicar(array1, array2);

		System.out.println("Array1");
		mostrarArray(array1);

		System.out.println("Array2");
		mostrarArray(array2);

		System.out.println("Array3");
		mostrarArray(array3);
	}

	public static void rellenar(int array[]) {
		for (int i = 0; i < array.length; i++) {
			array[i] = randomNum(1, 100);
		}
	}

	private static int randomNum(int min, int max) {
		return (int)(Math.random() * ((max - min) + 1)) + min;
	}

	public static int[] multiplicar(int[] array1, int array2[]) {
		int array3[] = new int[array1.length];

		for (int i = 0; i < array3.length; i++) {
			array3[i] = array1[i] * array2[i];
		}

		return array3;
	}

	public static void mostrarArray(int array[]) {
		for (int i = 0; i < array.length; i++) {
			System.out.println("Posicion " + i + " -> " + array[i]);
		}
	}
}
