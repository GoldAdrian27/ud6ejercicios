import java.util.Scanner;

public class mainApp {

	public static void main(String[] args) {
		
		Scanner teclado = new Scanner(System.in);
		
		System.out.println("Introduce la cantidad de numeros aleatorios quieres");
		int cantidad = teclado.nextInt();
		
		System.out.println("Introduce minimo del rango de los numeros aleatorios");
		int min = teclado.nextInt();
		
		System.out.println("Introduce maximo del rango de los numeros aleatorios");
		int max = teclado.nextInt();

		for (int i = 0; i < cantidad; i++) {
			System.out.println(numberGenerator(max, min));
		}

		
		
	}
	
	public static int numberGenerator(int max, int min) {
		
		return (int)(Math.random() * ((max - min) + 1)) + min;

	}
}
