import java.util.Scanner;

public class mainApp {

	public static void main(String[] args) {
		
		Scanner teclado = new Scanner(System.in);
		System.out.println("escribe un numero y dire si es primo o no");
		System.out.println(numeroPrimo(teclado.nextInt())?"Si es":"No es");
		
	}
	public static boolean numeroPrimo(int num) {
		int contador = 2;
		
		while ((contador < num)){
			if (num % contador == 0) {

				return false;
			}
			contador++;
		}
		return true;
	}

}
