import java.util.Scanner;

public class mainApp {

	public static void main(String[] args) {
		Scanner teclado = new Scanner(System.in);
		System.out.println("Introduce un numero para saber el factorial");
		System.out.println(calcularFactorial(teclado.nextInt()));
		
	}
	
	public static int calcularFactorial(int num) {
		int fact = 1;
		for (int i = 1; i <=num; i++) {
			fact*= i;
		}
		
		return fact;
	}
}
