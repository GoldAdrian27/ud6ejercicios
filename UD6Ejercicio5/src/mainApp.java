import java.util.Scanner;

public class mainApp {

	public static void main(String[] args) {
		Scanner teclado = new Scanner(System.in);
		System.out.println("Escribe un numero para pasarlo a binario");
		System.out.println(calcularBinario(teclado.nextInt()));
	}

	public static String calcularBinario(int num) {

		String binario = "";
		String binarioFinal = "";

		if(num != 0) {
			while (num != 0) {
				binario += num%2;
				num /= 2;
			}
		}else {
			return "0";
		}

		//Invertir String
		for (int i = binario.length()-1; i >= 0; i--) {

			binarioFinal += binario.charAt(i);
		}

		return binarioFinal;
	}
}
