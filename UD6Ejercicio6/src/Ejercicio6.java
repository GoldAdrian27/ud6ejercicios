import java.util.Scanner;

public class Ejercicio6 {

	public static void main(String[] args) {

		Scanner teclado = new Scanner(System.in);
		int num;
		do {
			System.out.println("Introduce un numero entero positivo");
			num = teclado.nextInt();
		}while(num < 0);
		
		System.out.println("Tiene " + cifras(num) + " cifras");
	}

	public static int cifras(int num) {
		//Paso el int a String y retorno el tama�o
		return Integer.toString(num).length();
		
	}

}
