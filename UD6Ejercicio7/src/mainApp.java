import java.util.Scanner;

public class mainApp {

	public static void main(String[] args) {

		Scanner teclado = new Scanner(System.in);
		System.out.println("introduce un numero");
		eurosToLibrasDolarYen(teclado.nextInt());
	}
	
	public static void eurosToLibrasDolarYen(int num) {
		
		double libras = num * 0.86;
		double dolar = num * 1.28511;
		double yenes = num * 129852;
		
		System.out.println(libras + " libras son " + num + "�");
		System.out.println(dolar + " dolares son " + num + "�");
		System.out.println(yenes + " yenes son " + num + "�");
	}
	
}
