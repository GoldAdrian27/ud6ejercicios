import java.util.Scanner;

public class mainApp {

	public static void main(String[] args) {
		int num[] = new int[10];
		rellenarArray(num);
		showArray(num);
		
	}
	public static void rellenarArray(int num[]) {
		Scanner teclado = new Scanner(System.in);
		for (int i = 0; i < num.length; i++) {
			System.out.println("Introduce un numero en la posicion " + i);
			num[i] = teclado.nextInt();
		}
	}
	public static void showArray(int num[]) {
		for (int i = 0; i < num.length; i++) {
			System.out.println(i + " -> " + num[i]);
			
		}
	}
}
