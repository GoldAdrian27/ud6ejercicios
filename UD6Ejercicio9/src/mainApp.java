import java.util.Scanner;

public class mainApp {

	public static void main(String[] args) {
		Scanner teclado = new Scanner(System.in);
		System.out.println("Introduce el  tama�o del array");
		int num[] = new int[teclado.nextInt()];
		rellenarArray(0, 9, num);
		mostrarSumar(num);
	}
	
	
	public static void rellenarArray(int max, int min, int num[]) {
		for (int i = 0; i < num.length; i++) {
			num[i] = randomNum(min, max);

		}
	}
	public static void mostrarSumar(int num[]) {
		int suma = 0;
		for (int i = 0; i < num.length; i++) {
			System.out.println("Posicion " + i + " -> " + num[i]);
			suma += num[i];
		}
		System.out.println("La suma de las posiciones es: " + suma);
	}
	
	private static int randomNum(int min, int max) {
		return (int)(Math.random() * ((max - min) + 1)) + min;
	}
}
